Source code for the paper 'H. Chen, C. Dong, L. Fan, G. Loukides, S. P. Pissis, and L. Stougie. Differentially Private String Sanitization for Frequency-Based Mining Tasks' 
published at ICDM 2021.


COMPILATION AND EXAMPLE
---------------------- 
All our methods as well as the baselines that are used in experiments 
compile and run on a small example with ./compile.txt 
We used g++ version (7.3.0) and tested our implementation on GNU/Linux.


INFORMATION ABOUT THE INPUT AND OUTPUT

MSH, FSH, and ASH heuristics
----------------------------

Input parameters (we refer to the parameters using the example in ./compile.txt):

    test.txt: This is the input multiplicity of each edge in DBG. Each line in this file represents an edge.
    epsilon: This is the parameter epsilon.
    delta: This is the parameter delta.
    minimum_edge_multiplicity: This is the minimum multiplicity of an edge that will be considered in the algorithm (if minimum_edge_multiplicity = 1, only edges with multiplicity larger than 1 will be considered.)
    tau: This is the parameter tau -- the minimum threshold of normalized multiplicity -- which is needed in to compute the F1 score.
    (Ratio zeta/zeta_max): This is the parameter used in the FSH algorithm, which we varied in [0.1*zeta_max, 1*zeta_max].

The output is displayed on the screen and includes the utility of the algorithm, JSD, L1 Distance, F1 score and runtime.
  
For example, when we execute './msh test.txt 0.5 0.5 1 0.0000006', epsilon = 0.5, delta = 0.5, minimum_edge_multiplicity = 1, and tau = 0.0000006.

The output of this command is the following:

JSD top k= 0.0266856154348223
L1 distance = 0.174291938997821
F1 = 0.842105263157895

Time taken : 0 miilliseconds


Examples for FSH and ASH are listed in compile.txt.

EDP 
----
  Input parameters (we refer to the parameters using the example in ./compile.txt):

    test.txt: This is the input multiplicity of each edge in DBG; each line represents an edge.
    epsilon: This is the parameter epsilon.
    delta: This is the parameter delta.
    tau: This is the parameter tau -- the minimum threshold of normalized multiplicity -- which is needed in to compute the F1 score.

The output is displayed on the screen and includes the utility of the algorithm, JSD, L1 Distance, and F1 score.

For example, the output for EDP when run 'Rscript edp.R test.txt 0.5 0.5 0.0000006' is the following: 

Read 11 items
Max_zeta= 27 
JSD = 0.004361669 
min L_1= 0.127451 at z= 21  out of  27  max_zeta has L1= 0.1742919 ratio last/best L1 1.367521 
F1 = 1 
Time difference of 0.1468751 secs



Classic Mechanisms
-------------------
   
   Input parameters (we refer to the parameters using the example in ./compile.txt):

    test.txt: This is the input multiplicity of each edge in DBG; each line represents an edge.
    epsilon: This is the parameter epsilon.
    delta: This is the parameter delta.
    tau: This is the parameter tau -- the minimum threshold of normalized multiplicity -- which is needed in to compute the F1 score.
    extra_zeros: This is the number of fake feasible edges.

The output is displayed on the screen and includes the utility of the algorithm, the mean of JSD, L1 Distance, and F1 score for 1000 runs.

For example, the output for Laplace when './laplace test.txt 0.5 0.5 1 0.0000006 15' runs is the following: 

mean L1: 0.291293
mean JSD: 0.0611139
mean F1: 0.777836

Example for Gaussian is listed in compile.txt.


Comments and Questions    
----------------------  

Grigorios Loukides: grigorios.loukides@kcl.ac.uk   

Huiping Chen: huiping.chen@kcl.ac.uk   



