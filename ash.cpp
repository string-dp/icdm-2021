#define DEBUG false
typedef long long INT;

#include <bits/stdc++.h>
#include <vector>
#include <fstream>
#include <time.h>
#include <cmath>
#include <chrono>
#include <cfloat>

using namespace std;
using namespace std::chrono;



void JSD_k(double *mul, double * rr, long long size)
{
	double *M_prime = new double[size];

	double mul_Mprime_kl=0.0;
	double rr_Mprime_kl=0.0;

	for(int i = 0;i<size;i++)
	{
		M_prime[i] = (double)(mul[i]+rr[i])/2;
		double temp =0.0;
		if(mul[i]!=0 && M_prime[i]!=0)
		{
			 temp=log2((double)mul[i]/(double)M_prime[i]);
			mul_Mprime_kl +=mul[i]*temp;
		}
		else mul_Mprime_kl +=0;
		double temp_rr=0.0;
		if(rr[i]!=0 && M_prime[i]!=0)
		{
			temp_rr=log2((double)rr[i]/(double)M_prime[i]);
			rr_Mprime_kl +=rr[i]*temp_rr;
		}
		else rr_Mprime_kl +=0;
	}
	double js =0.0;
	js =(double) (rr_Mprime_kl+mul_Mprime_kl)/(double)2;

	cout<<setprecision(15)<<"JSD = "<<js<<endl;
	delete []M_prime;
	return;
}

void precision_recall(double *mul, double * rr, long long size, double t)
{
	INT freq_after = 0;
	INT freq_before = 0;
	INT freq_both = 0;

	for(int i = 0;i<size;i++)
	{
		if(mul[i]>=t)
		{
			freq_before++;
		}
		if(rr[i]>=t)
		{
			freq_after++;
		}
		if(mul[i]>=t && rr[i]>=t)
		{
			freq_both++;
		}
	}
   double Precision =0;
   double Recall =0;
   if(freq_after>0)
   {
	   Precision =(double)freq_both/(double)freq_after;
   	}
	else 
	{
		cout<<"There is no edge freq >"<<t<<endl;
	}
	if(freq_before>0)
	{
		 Recall =(double)freq_both/(double)freq_before;
	}
	else {
		cout<<"There is no edge freq >"<<t<<" in the dataset"<<endl;
	}
	if((Precision+Recall)!=0)
	{
		cout<<"F1 = "<< (double)2*Precision*Recall/(double)(Precision+Recall)<<endl;
	}
	return;
}

void lDis(double *mul, double * rr, long long size)
{
	double sum= 0.0;
	double temp=0.0;
	for(int i = 0;i<size;i++)
	{
		temp=abs(mul[i]-rr[i]);
		sum+=temp;
	}
	cout<<setprecision(15)<<"L1 distance = "<<sum<<endl;
	return;
}


int main(int argc, char *argv[])
{

    ifstream edge_mult_file(argv[1]);  

    double epsilon=atof(argv[3]); 
    double delta=atof(argv[4]); 
    double gamma=min(epsilon,log(1/(1-delta)));
     int minimum_edge_multiplicity(atoi(argv[4])); 
	double t=atof(argv[5]);

    vector<INT> multiplicities;

    vector<vector<INT> > edge_multiplicity_x;
    unsigned long long  sum_mi_whole=0;
	int edge1_num =0;

     int lines_found=0;

     while(edge_mult_file)
     {
		string s;
		if(!getline(edge_mult_file, s)) break;

		lines_found++;
		istringstream ss( s );

		 while (ss)
		{
			  string s2;
			  if (!getline( ss, s2, ' ' )) break;
	
			sum_mi_whole+=(INT)stoi(s2);

			if((INT)stoi(s2)<=0)
			{
				cout<<"Edge input file must only have edges with multiplicity >=1!\n";
					exit(-1);
			}
		 if((INT)stoi(s2)<=minimum_edge_multiplicity)    {
			 edge1_num++;
		 }
		 if((INT)stoi(s2)>minimum_edge_multiplicity)
		{
			multiplicities.push_back((INT)stoi(s2));

			vector<INT> edge_multiplicity;
			edge_multiplicity.push_back((INT)stoi(s2));

			edge_multiplicity_x.push_back(edge_multiplicity);
		}
     }
}

	edge_mult_file.close();

    int number_of_vars=multiplicities.size();  
    INT *mi{ new INT[number_of_vars]{} };
    int cnt=0;
	sort(multiplicities.begin(), multiplicities.end(), greater <INT>());
    for(auto &it : multiplicities){
         mi[cnt++]=it;
	}

    auto ilp_start = high_resolution_clock::now();
    double *bi{ new double[number_of_vars]{} };

	double sum_mi=0.0;

	long long  size =number_of_vars+edge1_num;

	double *mul = new double[size];

   double* y=new double[number_of_vars];

    double y_0=-1.0;
    for(int i=0; i< number_of_vars; ++i)
    {
        bi[i]=log(mi[i]/(mi[i]-1.0));

      	sum_mi+=mi[i];
 	    mul[i]=(double)mi[i]/(double)sum_mi_whole;
		y[i]=mul[i];
        if(mul[i]*bi[i]/gamma>=y_0)
		y_0= mul[i]*bi[i]/gamma;
    }

	double sum_y_i=0.0;
    INT sum_xi=0;
	INT *cur_sol=new INT[size];

    for(int i=0; i<number_of_vars;++i)
    {
		INT temp=(INT)(y[i]/y_0);    
		double temp_dbl=y[i]/y_0;   

		INT x_i;
		if((temp_dbl >=temp+0.5) && (ceil(temp_dbl)*bi[i]<=gamma)) 
			x_i=temp+1;
		else
			x_i=temp;

		cur_sol[i]=x_i;
		sum_xi+=x_i;

		if(x_i>floor(gamma/bi[i]))
		{
			cout<<"Error constraint violation.\n";
			exit(-1);
		}
		
		sum_y_i+=temp_dbl;
	}

    auto ilp_end = high_resolution_clock::now();

	for(int i =number_of_vars;i<size;i++)
	{
		mul[i]=(double)1/(double)sum_mi_whole;
	}
   
	INT number_of_non_zero_xi =0;
	double *ilp_xi = new double[size];  
    for(int i=0; i<size;++i)
    {
		ilp_xi[i]=(double)cur_sol[i]/(double)sum_xi;
		if(cur_sol[i]>0){
			number_of_non_zero_xi ++;
		}

    }
	cout<<"--------------- Result ---------------"<<endl;
	if(number_of_non_zero_xi==0)
		 cout<<"All the value are 0s."<<endl;
	else 
		JSD_k(mul,ilp_xi,size);
	lDis(mul,ilp_xi,size);
	precision_recall(mul,ilp_xi,size,t);
	auto duration_ilp = duration_cast<milliseconds>(ilp_end - ilp_start);
	cout<<endl << "Time taken: " << duration_ilp.count() << " miilliseconds" << endl;
	
	delete []mul;
	delete []ilp_xi;
    delete []mi;
    delete []bi;
	delete []cur_sol;

	delete []y;


  return 0;
}
