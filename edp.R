args = commandArgs(trailingOnly=TRUE)
arg1<-args[1]
arg3<-as.numeric(args[2]) 
arg4<-as.numeric(args[3]) 
t<-as.numeric(args[4]) 

L1<-function(x,y)
{
  if(length(x)!=length(y))
  {
    cat("x and y in L1 have different lengths!")
    exit(-1);
  }
  return(abs(x-y))
}


our_JSD<-function(x,y)
{
  X<-x/sum(x);
  Y<-y/sum(y);
  M<-(X+Y)/2;
  
  if(length(X)!=length(Y))
  {
    cat("x and y don't have the same length")
    exit(-1);
  }
  
  sum1<-0;
  sum2<-0;
  for(i in 1:length(X)){
    temp1<-0;
    if(X[i]!=0 && M[i]!=0)
    {
      temp1<-log2(X[i]/M[i]);
      sum1<-sum1+X[i]*temp1;
    }
    else{
      sum1<-sum1+0;
    }
    
    if(Y[i]!=0 && M[i]!=0){
      temp2<-log2(Y[i]/M[i]);
      sum2<-sum2+Y[i]*temp2;
    }
    else{
      sum2<-sum2+0;
    }
  }
  #cat("sum1=",sum1," sum2=",sum2,"\n")
  return(0.5*(sum1+sum2))
}

prec_recall<-function(x,y,t)
{
  freq_after = 0;
  freq_before = 0;
  freq_both = 0;
  size<-length(x);
  
  X<-x/sum(x);
  Y<-y/sum(y);
  
  if(length(x)!=length(y))
  {
    cat("x and y in L1 have different lengths!")
    exit(-1);
  }
  
  for(i in 1:size){
    if(X[i]>=t){
      freq_before<-freq_before+1;
    }
    if(Y[i]>=t){
      freq_after<-freq_after+1;
    }
    if(X[i]>=t && Y[i]>=t){
      freq_both<-freq_both+1;
    }
  }
  Precision =0;
  Recall =0;
  if(freq_after>0){
    Precision =freq_both/freq_after;
  }
  else {
    cat("There is no edge freq >",t,"\n");
  }
  if(freq_before>0){
    Recall =freq_both/freq_before;
  }
  else {
    cat("There is no edge freq >",t," in the dataset\n");
  }
  if((Precision+Recall)!=0){ 
    F1<-((2*Precision*Recall)/(Precision+Recall));
    return(F1)
  }
  else{
    return(0);
  }
}

L_distances<-c()

M_all<-scan(arg1)

M<-M_all[which(M_all>1)]
M_one<-M_all[which(M_all==1)]

B<-log(M/(M-1))

gamma<-min(arg3,log(1/(1-arg4)))

max_zeta<-sum(floor(gamma/B))
cat("Max_zeta=",max_zeta,"\n")

n<-length(M)

start_time <- Sys.time()

F<-c()
for(i in 1:n)
  F[i]<-M[i]/sum(M_all)

for(zeta in 1:max_zeta)
{
  E<-matrix(0.0,nrow=zeta+1,ncol=n)
  A<-matrix(0.0,nrow=zeta+1,ncol=n)

  for(j in 1:(n-1))
  {
    E[1,j]<-sum(F[1:j])
    A[1,j]<-paste("-1",0)
  }
  E[1,n]<-10^9
  A[1,n]<-paste("-1",10^9)
  for(i in 1:zeta+1)
  {
    if(B[1]*(i-1)<=gamma)
    {
      E[i][1]<-abs(F[1]-(i-1)/(zeta))
      A[i][1]<-paste("-1",(i-1))
    }
    else
    {
      E[i][1]<-10^9
      A[i][1]<-paste("-1",10^9)
    }
  }
  for(i in 2:(zeta+1))
  {
    for(j in 2:n)
    {
    
      res<-c();
      min_ind<-10^9;
      min_r<-10^9;
      min_res<-10^9;
      for(r in 1:i)
      {
        if(B[j]*(r-1)<=gamma) 
        {
          res[r]<-E[i-r+1,j-1]+abs(F[j]-((r-1)/(zeta)))
        }
        else{
          res[r]<-10^10;
        }
        if(res[r]<min_res)
        {
            min_ind<-(i-r+1);
            min_r<-r;
            min_res=res[r];
        }
      }
      E[i,j]<-min_res
      A[i,j]<-paste(min_ind,(min_r-1))
    }
  }
  no_solution<-FALSE;
  solution<-c()
  
 j<-length(M)
 curr<-zeta+1;
 while(j!=0)
 {
   prev_cell<-as.numeric(substr(A[curr,j],1,which(strsplit(A[curr,j], "")[[1]]==" ")-1))
   
   value<-substr(A[curr,j],which(strsplit(A[curr,j], "")[[1]]==" ")+1,length(strsplit(A[curr,j],"")[[1]]))
   solution<-append(solution,as.numeric(value),0)
   curr<-prev_cell;
   
   if(value==1e09) 
   {
     cat("No solution!\n")
     no_solution<-TRUE;
     break;
   }
    if(prev_cell==-1 && j!=-1)
    {
      while(j>=2)
      {
        solution<-append(solution,0,after=0);
        j=j-1;
       
      } 
      
      break;
    }
   j<-j-1;
 }

  if(no_solution==FALSE)
  {
    normalized_solution<-solution/zeta
    L_distances<-append(L_distances,sum(L1(M/sum(M_all),normalized_solution)));  
  }
}
cat("JSD =", our_JSD(M,solution),"\n")
cat("min L_1=",min(L_distances)+length(M_one)*(1/sum(M_all)), "at z=",which.min(L_distances)," out of ",max_zeta, " max_zeta has L1=",L_distances[max_zeta]+length(M_one)*(1/sum(M_all)), "ratio last/best L1",(L_distances[max_zeta]+length(M_one)*(1/sum(M_all)))/(min(L_distances)+length(M_one)*(1/sum(M_all))),"\n")
cat("F1 =", prec_recall(M,solution,t),"\n")
end_time <- Sys.time()
end_time - start_time
start_time_lap <- Sys.time()


