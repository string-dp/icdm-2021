#define DEBUG false
typedef long long INT;

#include <bits/stdc++.h>
#include <vector>
#include <fstream>
#include <time.h>
#include <cmath>
#include <chrono>
#include <cfloat>
#include <random>

using namespace std;
using namespace std::chrono;
typedef vector<double> Vector;


void JSD_k(double *mul, double * rr, long long size)
{
	double *M_prime = new double[size];
	double mul_Mprime_kl=0.0;
	double rr_Mprime_kl=0.0;

	for(int i = 0;i<size;i++)
	{
		M_prime[i] = (double)(mul[i]+rr[i])/2;
		double temp =0.0;
		if(mul[i]!=0 && M_prime[i]!=0)
		{
			 temp=log2((double)mul[i]/(double)M_prime[i]);
			mul_Mprime_kl +=mul[i]*temp;
		}
		else mul_Mprime_kl +=0;
		double temp_rr=0.0;
		if(rr[i]!=0 && M_prime[i]!=0)
		{
			temp_rr=log2((double)rr[i]/(double)M_prime[i]);
			rr_Mprime_kl +=rr[i]*temp_rr;
		}
		else rr_Mprime_kl +=0;
	}
	double js =0.0;
	js =(double) (rr_Mprime_kl+mul_Mprime_kl)/(double)2;

	cout<<setprecision(15)<<"JSD top k= "<<js<<endl;
	delete []M_prime;
	return;
}


double JSD_k_returns(double *mul, double * rr, long long size){
	double *M_prime = new double[size];
	double mul_Mprime_kl=0.0;
	double rr_Mprime_kl=0.0;
	for(int i = 0;i<size;i++){
		M_prime[i] = (double)(mul[i]+rr[i])/2;
		double temp =0.0;
		if(mul[i]!=0 && M_prime[i]!=0){
			 temp=log2((double)mul[i]/(double)M_prime[i]);
			mul_Mprime_kl +=mul[i]*temp;
		}
		else mul_Mprime_kl +=0;
		double temp_rr=0.0;
		if(rr[i]!=0 && M_prime[i]!=0){
			temp_rr=log2((double)rr[i]/(double)M_prime[i]);
			rr_Mprime_kl +=rr[i]*temp_rr;
		}
		else rr_Mprime_kl +=0;
	}
	double js =0.0;
	js =(double) (rr_Mprime_kl+mul_Mprime_kl)/(double)2;
	delete []M_prime;
	return js;
}


double precision_recall(double *mul, double * rr, long long size, double t){
	INT freq_after = 0;
	INT freq_before = 0;
	INT freq_both = 0;

	for(int i = 0;i<size;i++){
		if(mul[i]>=t){
			freq_before++;
		}
		if(rr[i]>=t){
			freq_after++;
		}
		if(mul[i]>=t && rr[i]>=t){
			freq_both++;
		}
	}

   double Precision =0;
   double Recall =0;
   if(freq_after>0){
	   Precision =(double)freq_both/(double)freq_after;
   	}
	else {
		cout<<"There is no edge freq >"<<t<<endl;
	}
	if(freq_before>0){
		 Recall =(double)freq_both/(double)freq_before;
	}
	else {
		cout<<"There is no edge freq >"<<t<<" in the dataset"<<endl;
	}
	if((Precision+Recall)!=0)
	{	
		return (double)2*Precision*Recall/(double)(Precision+Recall);
	}
	else
	{
		cout<<"F1 undefined\n";
		return 0.0;
	}
	return 0.0;
}

void lDis(double *mul, double * rr, long long size){
	double sum= 0.0;
	double temp=0.0;

	for(int i = 0;i<size;i++){
		temp=abs(mul[i]-rr[i]);
		sum+=temp;
	}
	cout<<setprecision(15)<<"L1 distance = "<<sum<<endl;
	return;
}

double lDis_returns(double *mul, double * rr, long long size){
	double sum= 0.0;

	for(int i = 0;i<size;i++){
		sum+=abs(mul[i]-rr[i]);
	}
	return sum;
}


void Gauss(vector<INT> &edge_multiplicity,vector<INT> &LL_multiplicity, double epsilon, INT ub, normal_distribution<> & dist,  mt19937 &e2)
{
	INT sz_plus_1=ub; 
    for(auto &it : edge_multiplicity)
    {
		INT y=(it+dist(e2));
        if(y<0)
		    y=0;
		else if(y>sz_plus_1)
		    y=sz_plus_1;
        LL_multiplicity.push_back(round(y));
    }
}

int main(int argc, char *argv[])
{

    ifstream edge_mult_file(argv[1]); 
    double epsilon=atof(argv[2]); 
    double delta=atof(argv[3]); 
    double gamma=min(epsilon,log(1/(1-delta)));

    int minimum_edge_multiplicity(atoi(argv[4])); 
	double t=atof(argv[5]);
	int extra_zeros=atoi(argv[6]);
	
    vector<INT> multiplicities;
	vector<INT> lap_multiplicities;

	 vector<INT> my_mult;

   
    vector<vector<INT> > edge_multiplicity_x;
    unsigned long long  sum_mi_whole=0;
	int edge1_num =0;

     int lines_found=0;

     while(edge_mult_file)
     {
		string s;
		if(!getline(edge_mult_file, s)) break;
		lines_found++;
		istringstream ss( s );
		while (ss)
		{
			string s2;
			if (!getline( ss, s2, ' ' )) break;
			
			sum_mi_whole+=(INT)stoi(s2);
			my_mult.push_back((INT)stoi(s2));

			if((INT)stoi(s2)<=0)
			{
				cout<<"Edge input file must only have edges with multiplicity >=1!\n";
				exit(-1);
			}
			
			if((INT)stoi(s2)<=minimum_edge_multiplicity)
			{
				edge1_num++;
		 	}
			 if((INT)stoi(s2)>minimum_edge_multiplicity)
			{
				multiplicities.push_back((INT)stoi(s2));
				vector<INT> edge_multiplicity;
				edge_multiplicity.push_back((INT)stoi(s2));
				edge_multiplicity_x.push_back(edge_multiplicity);
			}
	     }
	}

	edge_mult_file.close();

    int number_of_vars=multiplicities.size();  //number of sufficiently frequent edges from file

    INT *mi{ new INT[number_of_vars]{} };
    int cnt=0;

	sort(multiplicities.begin(), multiplicities.end(), greater <INT>());

    for(auto &it : multiplicities){
         mi[cnt++]=it;
	}

    auto ilp_start = high_resolution_clock::now();

	vector<double> all_L1;
	vector<double> all_JSD;
	vector<double> all_F1;

	long long  size =number_of_vars+edge1_num+extra_zeros;

	double *mul = new double[size];
	for(INT i=0; i<number_of_vars+edge1_num;++i)
	{
		mul[i]=(double)my_mult.at(i)/(double)sum_mi_whole;
	}


	for(INT i=number_of_vars+edge1_num; i<size;++i)
	{	
		mul[i]=0; 
		my_mult.push_back(0);
	}

	int times=1000;
	for(int i=0;i<times;++i)
	{
		random_device rd;
		mt19937 e2(rd());
		normal_distribution<> dist(0, sqrt(2*log(1.25/delta))/epsilon);
		vector<INT> LL_multiplicity;

		Gauss(my_mult, LL_multiplicity, epsilon,sum_mi_whole, dist,e2);
		INT max_zeta=0;

		double *XIs = new double[size];


	  	for(int i=0;i<size;++i)
	  	{
			XIs[i]=LL_multiplicity.at(i);
	  	}
	  
		INT sum_xi=0;
		INT *cur_sol=new INT[size];

		for(INT i=0; i<size;++i)
		{		
			cur_sol[i]=XIs[i];
			sum_xi+=XIs[i];		
		}
		
		INT number_of_non_zero_xi =0;
		double *ilp_xi = new double[size];  

		for(int i=0; i<size;++i)
		{
			ilp_xi[i]=(double)cur_sol[i]/(double)sum_xi;
			if(cur_sol[i]>0){
				number_of_non_zero_xi ++;
			}
		}

	
	if(number_of_non_zero_xi==0)
		 cout<<"All the value are 0s."<<endl;
	else {
		all_JSD.push_back(JSD_k_returns(mul,ilp_xi,size));
	 }
	all_L1.push_back(lDis_returns(mul,ilp_xi,size));
	
	double F1=precision_recall(mul,ilp_xi,size,t);
	
	if(F1!=-1.0)
		all_F1.push_back(F1);
	delete []XIs;
	delete []ilp_xi;
    delete []cur_sol;
	
}
	delete []mul;

	double sum_L1=0.0;
	double sum_JS=0.0;
	double sum_F1=0.0;
	
	INT szz= all_L1.size();
    for(INT i=0;i<szz;++i)
	{
		sum_L1+=all_L1.at(i);
		sum_JS+=all_JSD.at(i);
	}
	
	INT szz_F1=all_F1.size();
	for(INT i=0;i<szz_F1;++i)
		sum_F1+=all_F1.at(i);
	
	cout<<"mean L1: "<<sum_L1/all_L1.size()<<endl;
	cout<<"mean JSD: "<<sum_JS/all_JSD.size()<<endl;
	cout<<"mean F1: "<<sum_F1/all_F1.size()<<endl;

  return 0;
}
